﻿using BuiTuanAnh_BTKiemTra.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BTKiemTra.Data
{
	public class ApplicationDbContext : IdentityDbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}
		public DbSet<Accounts> Accounts { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Employess> Employees { get; set; }
		public DbSet<Logs> Logs { get; set; }
		public DbSet<Reports> Reports { get; set; }
		public DbSet<Transactions> Transactions { get; set; }
	}
}
