﻿namespace BuiTuanAnh_BTKiemTra.Models
{
	public class Transactions
	{
		public int ID { get; set; }
		public string EmployeeID { get; set; }
		public string CustomorID { get; set; }
		public string Name { get; set; }
		public ICollection<Employess> Employees { get; set; }
	}
}
