﻿namespace BuiTuanAnh_BTKiemTra.Models
{
	public class Logs
	{
		public int ID { get; set; }
		public string TransactionalID { get; set; }
		public string LoginDate {  get; set; }
		public string LoginTime { get; set; }
		public ICollection<Transactions> Transactions { get; set; }
	}
}
