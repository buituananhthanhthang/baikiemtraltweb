﻿namespace BuiTuanAnh_BTKiemTra.Models
{
	public class Customer
	{
		public int ID { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ContractandAddress { get; set; }
		public string UsernName { get; set; }
		public string PassWord { get; set; }
		public Accounts Accounts { get; set; }
	}
}
