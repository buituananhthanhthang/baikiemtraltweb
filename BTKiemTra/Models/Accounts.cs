﻿namespace BuiTuanAnh_BTKiemTra.Models
{
	public class Accounts
	{
		public int ID { get; set; }
		public string CustomerID { get; set; }
		public string Accountname { get; set; }
		public ICollection<Customer> Customers { get; set; }
	}
}
