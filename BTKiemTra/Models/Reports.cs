﻿namespace BuiTuanAnh_BTKiemTra.Models
{
	public class Reports
	{
		public int ID { get; set; }
		public string AccountID { get; set; }
		public string LogsID { get; set; }
		public string TransactionalID { get; set; }
		public string ReportName { get; set; }
		public string ReportDate { get; set; }
		public ICollection<Transactions> Transactions { get; set; }
	}
}
